import React, { Component } from 'react';
import './App.css';
import { Switch } from 'react-router-dom';
import Principal from './Estructura/Principal';

export default class App extends Component {

  render() {
    return (
      <div>
        <Switch>
          <Principal />
        </Switch>
      </div>
    );
  }
}
