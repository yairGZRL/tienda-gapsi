import React, { Component } from 'react';
import Header from './Header';
import Content from './Content';

export default class Principal extends Component {

    // COMENTARIO PATRON DE DISEÑO WEB
    // En este componente se realizo el diseño web en base a diseño de bloques.
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <Header />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <Content />
                    </div>
                </div>
            </div>
        );
    }
}