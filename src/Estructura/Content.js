import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Home from '../Home/Home';

export default class Content extends Component {

    render() {
        return (
            <div className="row row-space-content">
                <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                    <Route exact path="/" component={Home} />
                </div>
            </div>
        );
    }
}