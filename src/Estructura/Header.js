import React, { Component } from 'react';

export default class Header extends Component {

    render() {
        return (
            <div className="row heade-superior">
                <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                    <img className="header-img" src="/img/logo.png" alt="Logo gapsi" />
                </div>
                <div className="col-12 col-sm-12 col-md-7 col-lg-7">
                    <h1><span className="badge badge-custom">e-Commerce Gapsi</span></h1>
                </div>
            </div>
        );
    }
}