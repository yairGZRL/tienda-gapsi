import React, { Component } from 'react';
import { URL_VISITOR } from '../Constants';
import axios from 'axios';

export default class Bienvenida extends Component {
    constructor(props) {
        super(props);
        this.state = {
            saludo: '',
            version: '',
        }
        this.bienvenida = this.bienvenida.bind(this);
    }

    componentDidMount() {
        this.bienvenida();
    }

    render() {
        return (
            <>
                <div className="col-12 col-sm-12 col-md-1 col-lg-1">
                    <img className="bienvenido-img" src="/img/coach.png" alt="Logo coach" />
                </div>
                <div className="col-12 col-sm-4 col-md-7 col-lg-7">
                    <h5>{this.state.saludo + " Version: " + this.state.version}</h5>
                </div>
            </>
        );
    }

    bienvenida() {
        axios.post(URL_VISITOR).then(result => {
            this.setState({ saludo: result.data.data.welcome, version: result.data.data.version });
        }).catch(error => {
            console.log("Error de comunicación con el servidor " + error.message)
        });
    }
}