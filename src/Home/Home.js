import React, { Component } from 'react';
import Busqueda from '../Productos/Busqueda';
import Bienvenida from './Bienvenida';
import ContenedorProducto from '../Productos/ContenedorProducto';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            valorBusqueda: '',
            limpiar: 0,
        }
        this.passList = this.passList.bind(this);
        this.limpiar = this.limpiar.bind(this);
    }

    // COMENTARIO PATRON DE DISEÑO WEB
    // En este componente se realizo el diseño web en base a diseño de bloques.
    render() {
        return (
            <>
                <div className="row">
                    <Bienvenida />
                </div>
                <div className="row">
                    <Busqueda list={this.passList} limpiar={this.limpiar}  />
                </div>
                <div className="row">
                    <ContenedorProducto list={this.state.list} valorBusqueda={this.state.valorBusqueda}
                     limpiar={this.state.limpiar}/>
                </div>
            </>
        );
    }

    passList(list, valorBusqueda) {
        this.setState({ list: list, valorBusqueda: valorBusqueda });
    }

    limpiar() {
        this.setState({ limpiar: this.state.limpiar + 1 });
    }
}