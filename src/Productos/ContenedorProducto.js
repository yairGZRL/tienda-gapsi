import React, { Component } from 'react';
import Lista from './Lista';
import Carrito from './Carrito';

export default class ContenedorProducto extends Component {

    // COMENTARIO PATRON DE DISEÑO WEB
    // En este componente se realizo el diseño web en base al patrón F
    render() {
        return (
            <>
                <div className="col-12 col-sm-12 col-md-8 col-lg-8 lista">
                    <Lista list={this.props.list} valorBusqueda={this.props.valorBusqueda}
                        limpiar={this.props.limpiar} />
                </div>
                <div className="col-12 col-sm-12 col-md-4 col-lg-4 lista">
                    <Carrito />
                </div>
            </>
        );
    }
}