import React, { Component } from 'react';

export default class Carrito extends Component {
    constructor(props) {
        super(props);
        this.dropHandler = this.dropHandler.bind(this);
        this.preventDefault = this.preventDefault.bind(this);
    }

    render() {
        return (
            <span id="contenedorCarrito" onDragOver={this.preventDefault()} onDrop={this.dropHandler()}
                onClick={this.handleClick} className="carrito">CARRITO DE COMPRA(Arrastra aqui los productos)</span>
        );
    }

    dropHandler = () => (event) => {
        event.preventDefault();
        var data = event.dataTransfer.getData("text");
        event.target.appendChild(document.getElementById(data));
    }

    preventDefault = () => (event) => {
        event.preventDefault();
    }
}