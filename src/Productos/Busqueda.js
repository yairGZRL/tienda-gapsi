import React, { Component } from 'react';
import { URL_PRODUCT } from '../Constants';
import axios from 'axios';
import { TextField, Button } from '@material-ui/core';

export default class Busqueda extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valorBusqueda: '',
        }
        this.busqueda = this.busqueda.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.limpiar = this.limpiar.bind(this);
    }

    render() {
        return (
            <>
                <div className="col-12 col-sm-12 col-md-3 col-lg-3"></div>
                <div className="col-12 col-sm-12 col-md-5 col-lg-5">
                    <TextField fullWidth id="valorBusqueda" name="valorBusqueda" label="Producto" value={this.state.valorBusqueda}
                        onChange={this.handleInputChange} />
                </div>
                <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                    <Button variant="contained" size="large" color="primary" onClick={this.busqueda}>
                        Buscar
                    </Button>
                </div>
                <div className="col-12 col-sm-4 col-md-2 col-lg-2">
                    <Button variant="contained" size="large" color="primary" onClick={this.limpiar}>
                        Reiniciar
                    </Button>
                </div>
            </>
        );
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    busqueda() {
        axios.get(URL_PRODUCT.concat(this.state.valorBusqueda).concat("/1"))
            .then(result => {
                this.props.list(result.data.data.products, this.state.valorBusqueda);
            }).catch(error => {
                console.log("Error de comunicación con el servidor " + error.message)
            });
    }

    limpiar() {
        this.setState({ valorBusqueda: '' });
        var container = document.getElementById("contenedorCarrito");
        var elements = container.getElementsByClassName("carrito-add");
        var child = elements.lastElementChild;
        while (child) {
            elements.removeChild(child);
            child = elements.lastElementChild;
        }
        this.props.limpiar();
    }
}