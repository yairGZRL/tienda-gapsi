import React, { Component } from 'react';
import { URL_PRODUCT } from '../Constants';
import axios from 'axios';

export default class Lista extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            page: 1,
            limpiador: false,
        }
        this.createProducts = this.createProducts.bind(this);
        this.dragHandler = this.dragHandler.bind(this);
        this.scrollHandler = this.scrollHandler.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.scrollHandler, true);
    }

    componentDidUpdate(prevProps) {
        if (this.props.list !== prevProps.list) {
            this.setState({ list: this.props.list })
        }
        if (this.props.limpiar !== prevProps.limpiar) {
            this.setState({ limpiador: true });

            var container = document.getElementById("tabla-principal");
            var elements = container.getElementsByClassName("border-producto");
            var child = elements.lastElementChild;
            while (child) {
                elements.removeChild(child);
                child = elements.lastElementChild;
            }

        } else if ((this.props.limpiar === prevProps.limpiar) && this.state.limpiador) {
            if (this.props.list !== prevProps.list) {
                this.setState({ limpiador: false });
            }
        }
    }

    render() {
        return (
            <div id="tabla-principal" className="col-12 col-sm-12 col-md-12 col-lg-12">
                {this.state.list.length > 1 ?
                    <div className="row header-tabla">
                        <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                            Imagen
                        </div>
                        <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                            SKU
                        </div>
                        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                            Descripcion
                        </div>
                        <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                            Precio
                        </div>
                    </div> : null}
                {this.createProducts()}
            </div>
        );
    }

    createProducts() {
        if (this.state.limpiador) {
            return <div></div>;
        } else {
            const rows = this.state.list.map((row) =>
                <div className="row border-producto" key={row.SKU} draggable="true"
                    onDragStart={this.dragHandler(row.NAME + "-" + row.SKU)} id={row.NAME + "-" + row.SKU}>
                    <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                        <img className="producto-img" src={row.IMAGE} alt="Imagen producto" />
                    </div>
                    <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                        {row.SKU}
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        {row.DESCRIPTION}
                    </div>
                    <div className="col-12 col-sm-12 col-md-2 col-lg-2">
                        {row.PRICE}
                    </div>
                </div>)
            return rows
        }
    }

    dragHandler = (idx) => (event) => {
        var element = document.getElementById(idx);
        element.classList.remove("border-producto");
        element.classList.add("carrito-add");
        event.dataTransfer.setData('text', idx);
    }

    scrollHandler = (event) => {
        if (event.target.scrollTop === event.target.scrollTopMax) {
            this.busqueda();
        }
    };

    busqueda() {
        const page = this.state.page + 1;
        axios.get(URL_PRODUCT.concat(this.props.valorBusqueda).concat("/").concat(page))
            .then(result => {
                var listAll = this.state.list.concat(result.data.data.products);
                this.setState({ list: listAll, page: page })
            }).catch(error => {
                console.log("Error de comunicación con el servidor " + error.message)
            });
    }
}